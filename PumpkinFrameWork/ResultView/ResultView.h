//
//  ResultView.h
//  PFSample
//
//  Created by Kasajima Yasuo on 12/03/04.
//  Copyright (c) 2012年 kyoto. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ResultViewTheme.h"

@interface ResultView : ResultViewTheme
@end
