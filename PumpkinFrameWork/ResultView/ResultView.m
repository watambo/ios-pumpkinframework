//
//  ResultView.m
//  PFSample
//
//  Created by Kasajima Yasuo on 12/03/04.
//  Copyright (c) 2012年 kyoto. All rights reserved.
//

#import "ResultView.h"
#import "PFHeader.h"
#import "SocialWebView.h"

@implementation ResultView

#pragma mark Life Cycle
-(void)startView{
    // Viewが現れたときに必ず呼ばれる。トランジションなどはここ。
    [self transitionMoveFromBelow:^{}];
    
}

-(void)createView{
    // Viewを構成するときに呼ばれる
    [self useTheme:Default]; // テーマを使う
    // テーマを使わない場合は上の一行をコメントアウトして以下に書く。ボタンの名前とうは下記メソッド参考
}





// Shareする内容を作る
#pragma mark SocialWeb
- (void)setSocialWebWithType:(enum SocialType)type{
    SocialWebView *webView = [SocialWebView socialWebViewWithType:type];
    NSString *status = [NSString stringWithFormat:NSLocalizedString(@"My Score is %lld", nil),[GameModel getScore]];
    [webView setStatus:status];
    [self addSubview:webView];
}

//これより以下は基本的には変更しない
#pragma mark press Btn
- (void)pressGameRestartBtn:(id)sender{
    [GameController playAudioFileName:@"PushBtn" ofType:@"wav"];
    [GameController gameReStart];
}

- (void)pressGameRetryBtn:(id)sender{
    [GameController gameRetry];
}

- (void)pressTweetBtn:(id)sender{
    [self setSocialWebWithType:Twitter];
}

- (void)pressMixiBtn:(id)sender{
    [self setSocialWebWithType:Mixi];
}

- (void)pressFacebookBtn:(id)sender{
    [self setSocialWebWithType:Facebook];
}

- (void)pressAmebaBtn:(id)sender{
    [self setSocialWebWithType:Ameba];
}

- (void)pressSaveShogoBtn:(id)sender{
    [GameModel saveValue:[GameModel getShogoForScore] withKey:@"shogo"];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
