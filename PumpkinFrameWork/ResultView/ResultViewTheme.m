//
//  ResultViewTheme.m
//  LittleGameSample
//
//  Created by Kasajima Yasuo on 12/03/13.
//  Copyright (c) 2012年 kyoto. All rights reserved.
//

#import "ResultViewTheme.h"

@implementation ResultViewTheme
#pragma mark DefaultTheme
- (void)defaultTheme{
    CGSize winSize = self.bounds.size;
    
    // 背景画像
    [self installImageViewWithFileName:@"ResultViewBgImg" inPosition:CGPointMake(0, 0)];
    
    // Back to StartView 透明で全体的に
    UIButton *toStartViewBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    toStartViewBtn.frame = self.bounds;
    [toStartViewBtn addTarget:self action:@selector(pressGameRestartBtn:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:toStartViewBtn];
    
    // Twitter
    [self installButtonNamed:@"TweetBtn" inPosition:CGPointMake(255, 380)];
    
    
    // 表示
    /*
    [self installLabelWithText:NSLocalizedString(@"SCORE",nil) atCenter:CGPointMake(winSize.width * 0.5, 120) size:20];
    
    NSString *scoreText = [NSString stringWithFormat:@"%lld %@",[GameModel getScore],SCORE_COUNT_NAME];
    [self installLabelWithText:scoreText atCenter:CGPointMake(winSize.width *0.5, 175) size:20];
    
    NSString *shogoText = [NSString stringWithFormat:NSLocalizedString(@"Title %@",nil),[GameModel getShogoForScore]];
    [self installLabelWithText:shogoText atCenter:CGPointMake(winSize.width * 0.5, 240) size:20];
    
    long long int difference = [GameModel getHighScore] - [GameModel getScore];
    NSString *string;
    if (difference<=0) {
        string = [NSString stringWithFormat:NSLocalizedString(@"You get HighScore!",nil)];
    }else{
        string = [NSString stringWithFormat:@"%d %@↓",difference,SCORE_COUNT_NAME];
    }
    [self installLabelWithText:string atCenter:CGPointMake(winSize.width * 0.5, 300) size:15];
    
    NSString *highScoreText = [NSString stringWithFormat:NSLocalizedString(@"HighScore  %lld　%@",nil),[GameModel getHighScore],SCORE_COUNT_NAME];
    [self installLabelWithText:highScoreText atCenter:CGPointMake(winSize.width * 0.5, 330) size:20];
     */
    
    [self installLabelWithText:NSLocalizedString(@"SCORE",nil) atCenter:CGPointMake(winSize.width * 0.5, 120) size:40];
    
    NSString *scoreText = [NSString stringWithFormat:@"%lld %@",[GameModel getScore],SCORE_COUNT_NAME];
    [self installLabelWithText:scoreText atCenter:CGPointMake(winSize.width *0.5, 175) size:50];
    
    NSString *shogoText = [NSString stringWithFormat:NSLocalizedString(@"Title %@",nil),[GameModel getShogoForScore]];
    [self installLabelWithText:shogoText atCenter:CGPointMake(winSize.width * 0.5, 240) size:20];
    
    long long int difference = [GameModel getHighScore] - [GameModel getScore];
    NSString *string;
    UIColor *color = FONT_COLOR;
    if (difference<=0) {
        string = [NSString stringWithFormat:NSLocalizedString(@"You get HighScore!",nil)];
        color = [UIColor redColor];
    }else{
        string = [NSString stringWithFormat:@"%d %@↓",difference,SCORE_COUNT_NAME];
    }
    UILabel *label = [self installLabelWithText:string atCenter:CGPointMake(winSize.width * 0.5, 280) size:15];
    label.textColor = color;
    
    
    NSString *highScoreText = [NSString stringWithFormat:NSLocalizedString(@"HighScore  %lld　%@",nil),[GameModel getHighScore],SCORE_COUNT_NAME];
    [self installLabelWithText:highScoreText atCenter:CGPointMake(winSize.width * 0.5, 330) size:30];
}


#pragma mark GrayScale
- (void)grayScaleTheme{
    CGSize winSize = self.bounds.size;
    
    // 背景画像
    UIImageView *backgroundImgView = [UIImageView imageViewWithFileName:@"ResutlViewBgImg" withColor:THEME_COLOR];
    [self addSubview:backgroundImgView];
    
    // 透明なGameRestartBtn
    [self installAlphaButtonName:@"GameRestartBtn" size:self.frame.size inPosition:CGPointMake(0, 0)];
    
    // Twitter
    [self installColoredButtonNamed:@"TweetBtn" inPosition:CGPointMake(255, 330)];
    
    // 表示
    NSString *labelText = [NSString stringWithFormat:NSLocalizedString(@"SCORE %lld %@",nil),[GameModel getScore],SCORE_COUNT_NAME];
    [self installLabelWithText:labelText atCenter:CGPointMake(winSize.width*0.5, winSize.height*0.5) size:30];
}



#pragma mark useTheme
- (void)useTheme:(enum ThemeType)type{
    switch (type) {
        case Default:
            [self defaultTheme];
            break;
        case GrayScale:
            [self grayScaleTheme];
        default:
            break;
    }
}

@end
