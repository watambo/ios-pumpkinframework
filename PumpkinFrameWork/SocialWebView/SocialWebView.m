//
//  SocialWebView.m
//  PFSample
//
//  Created by Kasajima Yasuo on 12/03/04.
//  Copyright (c) 2012年 kyoto. All rights reserved.
//

#import "SocialWebView.h"
#import "PFHeader.h"

@interface SocialWebView()

@end


@implementation SocialWebView

@synthesize type = _type;

#pragma mark Life Cycle
-(void)createView{
    // viewが構成されるときに呼ばれる
    [self useTheme:Default]; /// テーマを使う
    // テーマの記述は、PFCore>PFClass>PFWebViewに書いてある
    // テーマを使わない場合は対応していないので、もし変えたい場合はテーマを作る。
}

-(void)webViewDidStartLoad:(UIWebView *)webView{
    [super webViewDidStartLoad:webView];
    NSString *urlString = [webView.request.URL absoluteString];
    if([urlString hasPrefix:FACEBOOK_CALLBACK_URL]){
        [self performSelector:@selector(pressClosedBtn:)];
    }
}

-(void)webViewDidFinishLoad:(UIWebView *)webView{
    [super webViewDidFinishLoad:webView];
    NSString *urlString = [webView.request.URL absoluteString];
    if([urlString hasPrefix:FACEBOOK_CALLBACK_URL]){
        [self performSelector:@selector(pressClosedBtn:)];
    }
}

#pragma mark shareMethod
+(SocialWebView*)socialWebViewWithType:(enum SocialType)type{
    CGRect frame = [UIScreen mainScreen].bounds;
    frame.size.height -= kPFiPhoneAdSpace;
    SocialWebView *socialWebView = [[SocialWebView alloc] initWithFrame:frame];
    [socialWebView setPosition:CGPointMake(frame.origin.x, frame.size.height)];
    socialWebView.type = type;
    return [socialWebView autorelease];
}

- (void)setStatus:(NSString *)status{
    NSString *appUrl = APP_STORE_URL;
    NSString *hashTag = TWITTER_HASH_TAG;
    NSString *followMe = TWITTER_FOLLOW_ME;
    NSString *appName = APP_NAME;
    NSString *str;
    switch (_type) {
        case Twitter:
            str = [NSString stringWithFormat:@"https://twitter.com/intent/tweet?original_referer=%@&text=%@&url=%@&hashtags=%@&related=%@",appUrl,status,appUrl,hashTag,followMe];
            break;
        case Mixi:
            str = [NSString stringWithFormat:@"https://mixi.jp/simplepost/voice?status=%@ %@",status,appUrl];
            break;
        case Facebook:
            str = [NSString stringWithFormat:@"http://www.facebook.com/dialog/feed?app_id=%@&redirect_uri=%@&link=%@&caption=%@&description=%@&display=touch",FACEBOOL_APP_ID,FACEBOOK_CALLBACK_URL,appUrl,appName,status];
            break;
        case Ameba:
            str = [NSString stringWithFormat:@"http://now.ameba.jp/?entryText=%@ %@",status,appUrl];
        default:
            break;
    }
    NSString *encodedUrl = (NSString *) CFURLCreateStringByAddingPercentEscapes (NULL, (CFStringRef) str, NULL, NULL,kCFStringEncodingUTF8);
    NSURL *url = [NSURL URLWithString:encodedUrl];
    [encodedUrl release];
    NSURLRequest *req = [NSURLRequest requestWithURL:url];
    [self loadRequest:req];
}







@end
