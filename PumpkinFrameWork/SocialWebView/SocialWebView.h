//
//  SocialWebView.h
//  PFSample
//
//  Created by Kasajima Yasuo on 12/03/04.
//  Copyright (c) 2012年 kyoto. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PFWebView.h"
enum SocialType {
    Twitter,
    Mixi,
    Facebook,
    Ameba,
    };

@interface SocialWebView : PFWebView<UIWebViewDelegate>

@property enum SocialType type;

+(SocialWebView*)socialWebViewWithType:(enum SocialType)type;
-(void)setStatus:(NSString*)status;
@end
