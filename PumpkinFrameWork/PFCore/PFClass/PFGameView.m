//
//  PFGameView.m
//  PFSample
//
//  Created by Kasajima Yasuo on 12/03/05.
//  Copyright (c) 2012年 kyoto. All rights reserved.
//

#import "PFGameView.h"
#import "PFHeader.h"



@implementation PFGameView


#pragma mark createViewメソッド
- (UIButton *)installColoredButtonNamed:(NSString*)named inPosition:(CGPoint)position{
    UIButton *btn = [UIButton buttonWithImageFileName:named color:THEME_COLOR withDelegate:self];
    [btn setPosition:position];
    [self addSubview:btn];
    return btn;
}

- (UILabel *)installLabelWithText:(NSString *)text inPosition:(CGPoint)position size:(int)size{
    UILabel *label = [UILabel labelWithText:text];
    [label setPosition:position];
    [label setFont:[UIFont fontWithName:FONT_NAME size:size]];
    label.textColor = FONT_COLOR;
    [label reSize];
    [self addSubview:label];
    return label;
}

- (UILabel *)installLabelWithText:(NSString *)text atCenter:(CGPoint)center size:(int)size{
    UILabel *label = [UILabel labelWithText:text];
    label.center = center;
    [label setFont:[UIFont fontWithName:FONT_NAME size:size]];
    label.textColor = FONT_COLOR;
    [label reSizeFixCenter];
    [self addSubview:label];
    return label;
}

- (UIImageView *)installColoredImageViewWithFileName:(NSString *)fileName inPosition:(CGPoint)position{
    UIImageView *imageView = [UIImageView imageViewWithFileName:fileName withColor:THEME_COLOR];
    [imageView setPosition:position];
    [self addSubview:imageView];
    return imageView;
}

#pragma  mark LifeCycle
+(id)view{
    CGRect gameFrame = [UIScreen mainScreen].bounds;
    gameFrame.size.height -= kPFiPhoneAdSpace;
    NSLog(@"%f",gameFrame.size.width);
    id gameView = [[self alloc] initWithFrame:gameFrame];
    return [gameView autorelease];
}


- (void)startView{
    // 明示的に呼び出す
}

- (void)createView{
    // ここで初期化する
}

- (id)initWithFrame:(CGRect)frame{
    if ((self = [super initWithFrame:frame])) {
        self.clipsToBounds = YES;
        self.backgroundColor = THEME_COLOR;
        [self createView];
    }
    return self;
}

- (void)useTheme:(enum ThemeType)type{
   // テーマを書く
}

// startView関数内から呼び出される
#pragma mark transtion
- (void)transitionColorIn:(UIColor*)color{
    [self transitionColorIn:color completion:^{}];
}
- (void)transitionColorIn:(UIColor*)color completion:(PFAfeterTransition)afterTransition{
    int adSpace = 0;
    if ([kPFAdLocation isEqualToString:@"PortraitTop"]) {
        adSpace = -50;
    }
    UIImageView *imageView = [UIImageView imageViewWithImage:[GameModel getValueForKey:BEFORE_VIEW_KEY]];
    [imageView setPosition:CGPointMake(0, adSpace)];
    [self.superview addSubview:imageView];
    
    PFGameView *view = [PFGameView view];
    view.backgroundColor = color;
    [self.superview addSubview:view];
    view.alpha = 0;
    
    [UIView animateWithDuration:TRANSITION_TOTAL_TIME * 0.5
                     animations:^{
                         view.alpha = 1;
                     }
                     completion:^(BOOL finished){
                         [imageView removeFromSuperview];
                         [UIView animateWithDuration:TRANSITION_TOTAL_TIME * 0.5
                                          animations:^{
                                              view.alpha  = 0;
                                          } 
                                          completion:^(BOOL finished){
                                              [view removeFromSuperview];
                                              afterTransition();
                                          }
                          ];
                     }
     ];
}
- (void)transitionFadeIn:(PFAfeterTransition)afterTransition{
    int adSpace = 0;
    if ([kPFAdLocation isEqualToString:@"PortraitTop"]) {
        adSpace = -50;
    }
    UIImageView *imageView = [UIImageView imageViewWithImage:[GameModel getValueForKey:BEFORE_VIEW_KEY]];
    [imageView setPosition:CGPointMake(0, adSpace)];
    [self.superview addSubview:imageView];
    self.alpha = 0;
    [UIView animateWithDuration:TRANSITION_TOTAL_TIME 
                     animations:^{
                         self.alpha = 1;
                         imageView.alpha = 0;
                     } 
                     completion:^(BOOL finished){
                         [imageView removeFromSuperview];
                         afterTransition();
                     }
     ];
}

- (void)transitionWhiteIn:(PFAfeterTransition)afterTransition{
    [self transitionColorIn:[UIColor whiteColor] completion:afterTransition];
}

- (void)transitionBlackIn:(PFAfeterTransition)afterTransition;{
    [self transitionColorIn:[UIColor blackColor] completion:afterTransition];
}

- (void)transitionMove:(CGPoint)point  completion:(PFAfeterTransition)afterTransition{
    int adSpace = 0;
    if ([kPFAdLocation isEqualToString:@"PortraitTop"]) {
        adSpace = -50;
    }
    
    UIImageView *imageView = [UIImageView imageViewWithImage:[GameModel getValueForKey:BEFORE_VIEW_KEY]];
    [imageView setPosition:CGPointMake(0, adSpace)];
    
    [self.superview addSubview:imageView];
    
    [self setPosition:CGPointMake(point.x * self.frame.size.width, point.y * self.frame.size.height)];
    [UIView animateWithDuration:TRANSITION_TOTAL_TIME
                     animations:^{
                         [self setPosition:CGPointMake(0, 0)];
                         [imageView setPosition:CGPointMake(- point.x * self.frame.size.width, - point.y *self.frame.size.height +adSpace)];
                     }
                     completion:^(BOOL finished){
                         [imageView removeFromSuperview];
                         afterTransition();
                     }
     ];
}

- (void)transitionMoveFromBelow:(PFAfeterTransition)afterTransition{
    [self transitionMove:CGPointMake(0, -1) completion:(PFAfeterTransition)afterTransition];
} 

- (void)transitionMoveFromUpper:(PFAfeterTransition)afterTransition{
    [self transitionMove:CGPointMake(0, 1) completion:(PFAfeterTransition)afterTransition];
}

- (void)transitionMoveFromLeft:(PFAfeterTransition)afterTransition{
    [self transitionMove:CGPointMake(-1, 0) completion:(PFAfeterTransition)afterTransition];
}

- (void)transitionMoveFromRight:(PFAfeterTransition)afterTransition{
    [self transitionMove:CGPointMake(1, 0) completion:(PFAfeterTransition)afterTransition];
}


@end
