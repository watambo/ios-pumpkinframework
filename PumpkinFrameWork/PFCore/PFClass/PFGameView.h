//
//  PFGameView.h
//  PFSample
//
//  Created by Kasajima Yasuo on 12/03/05.
//  Copyright (c) 2012年 kyoto. All rights reserved.
//

#import "PFView.h"

enum ThemeType {
    Default,
    GrayScale,
};

typedef void (^PFAfeterTransition)();

@interface PFGameView : PFView
- (UIButton *)installColoredButtonNamed:(NSString*)named inPosition:(CGPoint)position;
- (UILabel *)installLabelWithText:(NSString *)text inPosition:(CGPoint)position size:(int)size;
- (UILabel *)installLabelWithText:(NSString *)text atCenter:(CGPoint)center size:(int)size;
- (UIImageView *)installColoredImageViewWithFileName:(NSString *)fileName inPosition:(CGPoint)position;
+(id)view;
- (void)startView;
- (void)createView;
- (void)useTheme:(enum ThemeType)type;

#pragma  mark transtion
- (void)transitionColorIn:(UIColor*)color completion:(PFAfeterTransition)afterTransition;
- (void)transitionFadeIn:(PFAfeterTransition)afterTransition;
- (void)transitionWhiteIn:(PFAfeterTransition)afterTransition;
- (void)transitionBlackIn:(PFAfeterTransition)afterTransition;
- (void)transitionMoveFromBelow:(PFAfeterTransition)afterTransition;
- (void)transitionMoveFromUpper:(PFAfeterTransition)afterTransition;
- (void)transitionMoveFromLeft:(PFAfeterTransition)afterTransition;
- (void)transitionMoveFromRight:(PFAfeterTransition)afterTransition;
@end
