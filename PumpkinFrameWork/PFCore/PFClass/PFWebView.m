//
//  PFWebView.m
//  LittleGameSample
//
//  Created by Kasajima Yasuo on 12/03/12.
//  Copyright (c) 2012年 kyoto. All rights reserved.
//

#import "PFWebView.h"
#import "PFHeader.h"
#import "IndicatorController.h"

@implementation PFWebView

#pragma mark createViewメソッド

- (void)installColoredButtonNamed:(NSString*)named inPosition:(CGPoint)position{
    UIButton *btn = [UIButton buttonWithImageFileName:named color:THEME_COLOR withDelegate:self];
    [btn setPosition:position];
    [self addSubview:btn];
}

#pragma mark UIWebView Delagate
// webをロード開始
-(void)webViewDidStartLoad:(UIWebView *)webView{
    CGRect winSize = [UIScreen mainScreen].bounds;
    winSize.size.height -= kPFiPhoneAdSpace;
    CGRect frame = winSize;
    if (self.frame.origin.y > frame.origin.y) {
        [UIView animateWithDuration:TRANSITION_TOTAL_TIME animations:^{
            self.frame = frame;
        }];
    }
    [IndicatorController startIndicator];
}

// webロード終了
-(void)webViewDidFinishLoad:(UIWebView *)webView{
    [IndicatorController stopIndicator];
}


#pragma mark PressBtn
-(void)pressClosedBtn:(id)sender{
    [IndicatorController stopIndicator];
    [UIView animateWithDuration:TRANSITION_TOTAL_TIME
                     animations:^{
                         CGRect winSize = [UIScreen mainScreen].bounds;
                         winSize.size.height -= kPFiPhoneAdSpace;
                         
                         CGRect frame = winSize;
                         frame.origin.y = frame.size.height;
                         self.frame = frame;
                     }
                     completion:^(BOOL finished){
                         [self removeFromSuperview];
                     }
     ];
}

- (void)pressGoBackBtn:(id)sender{
    [self goBack];
}

- (void)pressGoFowardBtn:(id)sender{
    [self goForward];
}

- (void)pressReloadBtn:(id)sender{
    [self reload];
}


- (void)defaultTheme{
    int btnHeight = 380;
    [self installButtonNamed:@"ClosedBtn" inPosition:CGPointMake(15, btnHeight)];
    [self installButtonNamed:@"GoBackBtn" inPosition:CGPointMake(15+30+50, btnHeight)];
    [self installButtonNamed:@"GoFowardBtn" inPosition:CGPointMake(15+30*2+50*2, btnHeight)];
    [self installButtonNamed:@"ReloadBtn" inPosition:CGPointMake(15+30*3+50*3, btnHeight)];
}

- (void)grayScaleTheme{
    int btnHeight = 380;
    [self installColoredButtonNamed:@"ClosedBtn" inPosition:CGPointMake(15, btnHeight)];
    [self installColoredButtonNamed:@"GoBackBtn" inPosition:CGPointMake(15+30+50, btnHeight)];
    [self installColoredButtonNamed:@"GoFowardBtn" inPosition:CGPointMake(15+30*2+50*2, btnHeight)];
    [self installColoredButtonNamed:@"ReloadBtn" inPosition:CGPointMake(15+30*3+50*3, btnHeight)];
}

-(void)useTheme:(enum ThemeType)type{
    switch (type) {
        case Default:
            [self defaultTheme];
            break;
        case GrayScale:
            [self grayScaleTheme];
            break;
        default:
            break;
    }
}

+(id)view{
    CGRect gameFrame = [UIScreen mainScreen].bounds;
    gameFrame.size.height -= kPFiPhoneAdSpace;
    PFWebView* gameView = [[self alloc] initWithFrame:gameFrame];
    [gameView setPosition:CGPointMake(0, gameView.bounds.size.height)];
    return [gameView autorelease];
}

- (void)createView{
    
}

- (id)initWithFrame:(CGRect)frame{
    if ((self = [super initWithFrame:frame])) {
        self.delegate = self;
        self.scalesPageToFit =  YES;
        [self createView];
        
        [[NSNotificationCenter defaultCenter] addObserver: self 
                                                 selector: @selector(enteredBackground:) 
                                                     name: @"didEnterBackground" 
                                                   object: nil];
    }
    return self;
}

-(void)enteredBackground:(UIApplication *)application{
    [IndicatorController stopIndicator];
}

@end
