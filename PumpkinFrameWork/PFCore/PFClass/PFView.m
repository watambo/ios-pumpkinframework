//
//  PFGameView.m
//  PFSample
//
//  Created by Kasajima Yasuo on 12/03/04.
//  Copyright (c) 2012年 kyoto. All rights reserved.
//

#import "PFView.h"

@implementation PFView


#pragma mark タイマー
- (NSTimer*)startTimer:(SEL)selector interval:(float)interval withUserInfo:(id)userInfo{
    [self stopTimer:selector];
    
    NSTimer *tm = [NSTimer scheduledTimerWithTimeInterval:interval 
                                                   target:self 
                                                 selector:selector 
                                                 userInfo:userInfo
                                                  repeats:YES];
    
    [timers setValue:tm forKey:NSStringFromSelector(selector)];
    return tm;
}

- (NSTimer*)startTimer:(SEL)selector interval:(float)interval{
    return [self startTimer:selector interval:interval withUserInfo:nil];
}

- (NSTimer*)startTimer:(SEL)selector{
    return [self startTimer:selector interval:1.0/60.0];
}

- (void)stopTimer:(SEL)selector{
    NSString *key = NSStringFromSelector(selector);
    NSTimer *tm = (NSTimer*)[timers valueForKey:key];
    [timers removeObjectForKey:key];
    [tm invalidate];
}


- (void)stopAllTimer{
    NSArray *timesAry = [timers allValues];
    for (NSObject *tm in timesAry) {
        if ([tm isKindOfClass:[NSTimer class]]) {
            [(NSTimer*)tm invalidate];
        }
    }
    [timers removeAllObjects];
}

-(void)removeFromSuperview{
    [self stopAllTimer];
    [super removeFromSuperview];
}


- (void)dealloc {
    [self stopAllTimer];
    [timers release];
    [super dealloc];
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        timers = [[NSMutableDictionary alloc] init];
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
