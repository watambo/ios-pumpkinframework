//
//  PFGameSubView.h
//  LittleGameSample
//
//  Created by Kasajima Yasuo on 12/03/14.
//  Copyright (c) 2012年 kyoto. All rights reserved.
//

#import "PFGameView.h"

@interface PFGameSubView : PFGameView
- (void)removeFromSuperviewWithTransitionColor:(UIColor*)color completion:(PFAfeterTransition)afterTransition;
- (void)removeFromSuperviewWithTransitionFadeOut:(PFAfeterTransition)afterTransition;
- (void)removeFromSuperviewWithTransitionWhiteOut:(PFAfeterTransition)afterTransition;
- (void)removeFromSuperviewWithTransitionBlackOut:(PFAfeterTransition)afterTransition;
- (void)removeFromSuperviewWithTransitionMoveBelow:(PFAfeterTransition)afterTransition;
- (void)removeFromSuperviewWithTransitionMoveUpper:(PFAfeterTransition)afterTransition;
- (void)removeFromSuperviewWithTransitionMoveLeft:(PFAfeterTransition)afterTransition;
- (void)removeFromSuperviewWithTransitionMoveRight:(PFAfeterTransition)afterTransition;
@end
