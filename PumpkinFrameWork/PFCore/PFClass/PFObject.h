//
//  PFObject.h
//  PFSample
//
//  Created by Kasajima Yasuo on 12/03/05.
//  Copyright (c) 2012年 kyoto. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PFObject : NSObject{
    NSMutableDictionary *timers;
}
- (NSTimer*)startTimer:(SEL)selector interval:(float)interval withUserInfo:(id)userInfo;
- (NSTimer*)startTimer:(SEL)selector interval:(float)interval;
- (NSTimer*)startTimer:(SEL)selector;
- (void)stopTimer:(SEL)selector;
- (void)stopAllTimer;

@end
