//
//  PFGameSubView.m
//  LittleGameSample
//
//  Created by Kasajima Yasuo on 12/03/14.
//  Copyright (c) 2012年 kyoto. All rights reserved.
//

#import "PFGameSubView.h"

@implementation PFGameSubView

- (void)transitionColorIn:(UIColor *)color completion:(PFAfeterTransition)afterTransition{
    self.alpha = 0;
    PFGameView *view = [PFGameView view];
    view.backgroundColor = [UIColor whiteColor];
    [self.superview addSubview:view];
    view.alpha = 0;
    
    [UIView animateWithDuration:TRANSITION_TOTAL_TIME * 0.5
                     animations:^{
                         view.alpha = 1;
                     } 
                     completion:^(BOOL finished){
                         self.alpha = 1;
                         [UIView animateWithDuration:TRANSITION_TOTAL_TIME * 0.5
                                          animations:^{
                                              view.alpha = 0;
                                              
                                          } 
                                          completion:^(BOOL finished){
                                              [view removeFromSuperview];
                                              afterTransition();
                                          }
                          ];
                     }
     ];
}

- (void)transitionFadeIn:(PFAfeterTransition)afterTransition{
    self.alpha = 0;
    [UIView animateWithDuration:TRANSITION_TOTAL_TIME 
                     animations:^{
                         self.alpha = 1;
                     } 
                     completion:^(BOOL finished){
                         afterTransition();
                     }
     ];
}

- (void)transitionWhiteIn:(PFAfeterTransition)afterTransition{
    [self transitionColorIn:[UIColor whiteColor] completion:afterTransition];
}

- (void)transitionBlackIn:(PFAfeterTransition)afterTransition{
    [self transitionColorIn:[UIColor blackColor] completion:afterTransition];
}


#pragma mark removeFronSuperview
- (void)removeFromSuperviewWithTransitionColor:(UIColor*)color completion:(PFAfeterTransition)afterTransition{
    PFGameView *view = [PFGameView view];
    
    view.backgroundColor = color;
    [self addSubview:view];
    view.alpha = 0;
    [UIView animateWithDuration:TRANSITION_TOTAL_TIME * 0.5
                     animations:^{
                         view.alpha = 1;
                     } 
                     completion:^(BOOL finished){
                         [UIView animateWithDuration:TRANSITION_TOTAL_TIME * 0.5
                                          animations:^{
                                              self.alpha = 0;
                                          } 
                                          completion:^(BOOL finished){
                                              [view removeFromSuperview];
                                              [self removeFromSuperview];
                                              afterTransition();
                                          }
                          ];
                     }
     ];
}
- (void)removeFromSuperviewWithTransitionFadeOut:(PFAfeterTransition)afterTransition{
    
    [UIView animateWithDuration:TRANSITION_TOTAL_TIME 
                     animations:^{
                         self.alpha = 0;
                     } 
                     completion:^(BOOL finished){
                         [self removeFromSuperview];
                         afterTransition();
                     }
     ];
    
}


- (void)removeFromSuperviewWithTransitionWhiteOut:(PFAfeterTransition)afterTransition{
    [self removeFromSuperviewWithTransitionColor:[UIColor whiteColor] completion:afterTransition];
}

- (void)removeFromSuperviewWithTransitionBlackOut:(PFAfeterTransition)afterTransition{
    [self removeFromSuperviewWithTransitionColor:[UIColor blackColor] completion:afterTransition];
}


#pragma mark transition Move
- (void)transitionMove:(CGPoint)point completion:(PFAfeterTransition)afterTransition{
    [self setPosition:CGPointMake(point.x * self.frame.size.width, point.y * self.frame.size.height)];
    [UIView animateWithDuration:TRANSITION_TOTAL_TIME
                     animations:^{
                         [self setPosition:CGPointMake(0, 0)];
                     }
                     completion:^(BOOL finished){
                         afterTransition();
                     }
     ];
}

- (void)transitionMoveFromBelow:(PFAfeterTransition)afterTransition{
    [self transitionMove:CGPointMake(0, -1) completion:afterTransition];
}

- (void)transitionMoveFromUpper:(PFAfeterTransition)afterTransition{
    [self transitionMove:CGPointMake(0, 1) completion:afterTransition];
}

- (void)transitionMoveFromLeft:(PFAfeterTransition)afterTransition{
    [self transitionMove:CGPointMake(-1, 0) completion:afterTransition];
}

- (void)transitionMoveFromRight:(PFAfeterTransition)afterTransition{
    [self transitionMove:CGPointMake(1, 0) completion:afterTransition];
}

- (void)removeFromSuperviewWithTransitionMove:(CGPoint)point completion:(PFAfeterTransition)afterTransition{
    [UIView animateWithDuration:TRANSITION_TOTAL_TIME
                     animations:^{
                         [self setPosition:CGPointMake(- point.x * self.frame.size.width, - point.y *self.frame.size.height)];
                     }
                     completion:^(BOOL finished){
                         [self removeFromSuperview];
                         afterTransition();
                     }
     ];
}

- (void)removeFromSuperviewWithTransitionMoveBelow:(PFAfeterTransition)afterTransition{
    [self removeFromSuperviewWithTransitionMove:CGPointMake(0, -1) completion:afterTransition];
}

- (void)removeFromSuperviewWithTransitionMoveUpper:(PFAfeterTransition)afterTransition{
    [self removeFromSuperviewWithTransitionMove:CGPointMake(0, 1) completion:afterTransition];
}

- (void)removeFromSuperviewWithTransitionMoveLeft:(PFAfeterTransition)afterTransition{
    [self removeFromSuperviewWithTransitionMove:CGPointMake(-1, 0) completion:afterTransition];
}

- (void)removeFromSuperviewWithTransitionMoveRight:(PFAfeterTransition)afterTransition{
    [self removeFromSuperviewWithTransitionMove:CGPointMake(1, 0) completion:afterTransition];
}


@end
