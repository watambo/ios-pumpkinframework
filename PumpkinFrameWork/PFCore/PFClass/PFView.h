//
//  PFGameView.h
//  PFSample
//
//  Created by Kasajima Yasuo on 12/03/04.
//  Copyright (c) 2012年 kyoto. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PFHeader.h"
#import "UIKitHelper.h"

@interface PFView : UIView{
    NSMutableDictionary *timers;
}
- (NSTimer*)startTimer:(SEL)selector interval:(float)interval withUserInfo:(id)userInfo;
- (NSTimer*)startTimer:(SEL)selector interval:(float)interval;
- (NSTimer*)startTimer:(SEL)selector;
- (void)stopTimer:(SEL)selector;
- (void)stopAllTimer;
@end
