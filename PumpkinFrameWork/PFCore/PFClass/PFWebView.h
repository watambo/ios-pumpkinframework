//
//  PFWebView.h
//  LittleGameSample
//
//  Created by Kasajima Yasuo on 12/03/12.
//  Copyright (c) 2012年 kyoto. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PFGameView.h"

@interface PFWebView : UIWebView <UIWebViewDelegate>
- (void)installColoredButtonNamed:(NSString*)named inPosition:(CGPoint)position;
- (void)createView;
- (void)useTheme:(enum ThemeType)type;
+(id)view;
@end
