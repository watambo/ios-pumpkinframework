//
//  GameModel.m
//  PFSample
//
//  Created by Kasajima Yasuo on 12/03/03.
//  Copyright (c) 2012年 kyoto. All rights reserved.
//

#import "GameModel.h"
#import "Parameter.h"
#import "GameKitController.h"
#import "UIKitHelper.h"
#import "GameController.h"



#define STATUS @"Status"


@implementation GameModel

@synthesize dict = _dict;
@synthesize shogoAry = _shogoAry;

static GameModel *instanceOfGameModel = nil;

#pragma mark Class Method
+ (GameModel*)sharedGameModel{
    if (!instanceOfGameModel) {
        instanceOfGameModel = [[GameModel alloc] init];
    }
    return instanceOfGameModel;
}

#pragma mark Dic
+ (void)saveValue:(id)value withKey:(NSString*)key{
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    [ud setValue:value forKey:key];
    [ud synchronize];
}

+ (void)saveInteger:(int)value withKey:(NSString*)key{
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    [ud setInteger:value forKey:key];
    [ud synchronize];
}

+ (void)saveDouble:(double)value withKey:(NSString*)key{
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    [ud setDouble:value forKey:key];
    [ud synchronize];
}

+ (void)saveLongLong:(long long int)value withKey:(NSString*)key{
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    NSNumber *number = [NSNumber numberWithLongLong:value];
    [ud setValue:number forKey:key];
    [ud synchronize];
}

+ (void)saveImage:(UIImage *)image withKey:(NSString*)key{
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    [ud setImage:image forKey:key];
}

+ (id)savedValueForKey:(NSString*)key{
    return [[NSUserDefaults standardUserDefaults] valueForKey:key];
}

+ (int)savedIntegerForKey:(NSString*)key{
    return [[NSUserDefaults standardUserDefaults] integerForKey:key];
}

+ (float)savedDoubleForKey:(NSString*)key{
    return [[NSUserDefaults standardUserDefaults] doubleForKey:key];
}

+ (long long)savedLongLongForKey:(NSString*)key{
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    long long int longlongInt  = [[ud valueForKey:key] longLongValue];
    return longlongInt;
}

+ (UIImage*)savedImageForKey:(NSString*)key{
    return [[NSUserDefaults standardUserDefaults] imageForKey:key];
}

// dic
+ (void)setValue:(id)value withKey:(NSString*)key{
    [[GameModel sharedGameModel].dict setValue:value forKey:key];
}

+ (void)setInteger:(int)value withKey:(NSString*)key{
    [[GameModel sharedGameModel].dict setInteger:value forKey:key];
}

+ (void)setDouble:(double)value withKey:(NSString*)key{
    [[GameModel sharedGameModel].dict setDouble:value forKey:key];
}

+ (void)setLongLong:(long long int)value withKey:(NSString*)key{
    [[GameModel sharedGameModel].dict setLongLong:value forKey:key];
}

+ (id)getValueForKey:(NSString*)key{
    return [[GameModel sharedGameModel].dict valueForKey:key];
}

+ (int)getIntegerForKey:(NSString*)key{
    return [[GameModel sharedGameModel].dict integerForKey:key];
}

+ (float)getDoubleForKey:(NSString*)key{
    return [[GameModel sharedGameModel].dict doubleForKey:key];
}

+ (long long)getLongLongForKey:(NSString*)key{
    return [[GameModel sharedGameModel].dict longlongForKey:key];
}


#pragma mark Score
// ハイスコアを返す
+ (long long int)getHighScore{
    return [GameModel savedLongLongForKey:HIGH_SCORE_KEY];
}

// ハイスコアをセットする
+ (void)setHighScore:(long long int)score{
    long long int highScore = [GameModel getHighScore];
    if ([SCORE_SORT_TYPE isEqualToString:@"ASC"]){
        if (highScore>score || highScore == 0)
            [GameModel saveLongLong:score withKey:HIGH_SCORE_KEY];
    }else{
        if (highScore<score)
            [GameModel saveLongLong:score withKey:HIGH_SCORE_KEY];
    }
    
    // GameCenterに送る
    [[GameKitController sharedGameKitController] submitScoreWithScore:score categoryNum:GCLHgighscore];
}

// スコア
+ (long long int)getScore{
    return [GameModel getLongLongForKey:SCORE_KEY];
}

+ (void)setScore:(long long int)score{
    [GameModel setLongLong:score withKey:SCORE_KEY];
}

+ (void)updateScore:(long long int)score{
    long long int beforeScore = [GameModel getScore];
    [GameModel setScore:beforeScore + score];
}


// Timer
+ (double)getTime{
    return [GameModel getDoubleForKey:TIME_KEY];
}

+ (void)updateTime:(double)time{
    double newTime = [self getTime] + time;
    [GameModel setDouble:newTime withKey:TIME_KEY];
}

+ (void)resetTime{
    [GameModel setDouble:0 withKey:TIME_KEY];
}

// 回数
+ (long long int)getPlayCount{
    return [GameModel getLongLongForKey:PLAY_COUNT_KEY];
}

+ (void)updatePlayCount{
    int newPlayCount = [GameModel savedLongLongForKey:PLAY_COUNT_KEY]+1;
    [GameModel saveLongLong:newPlayCount withKey:PLAY_COUNT_KEY];
}

#pragma mark Shogo
// 称号
- (void)setShogo{
    NSString *path;
    if ([GameController isJapan]) {
        path= [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"shogo_ja.txt"];
    }else{
        path= [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"shogo.txt"];
    }
    
    if (!path) { // 日本語が見当たらなかった場合
        path= [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"shogo.txt"];
    }
    
    // shogo.txtの中身をNSStringに入れる
    NSString *shogoText = [NSString stringWithContentsOfFile:path encoding:NSUTF8StringEncoding error:nil];
    // 改行で分割してArrayに入れる。
    NSArray *lineArray = [shogoText componentsSeparatedByString:@"\n"];
    
    // shogoArrayの初期化
    self.shogoAry = [[[NSMutableArray alloc] init] autorelease];
    // lineArrayを一つづつ中身を取り出す
    for (int i = 0; i <lineArray.count; i++) {
        // lineArrayを「,」で分割
        NSArray *lineComponent = [[lineArray objectAtIndex:i] componentsSeparatedByString:@","];
        if (lineComponent.count != 2) 
            break;
        // 最初のコンポーネントがスコアになる
        NSString *scoreString = [lineComponent objectAtIndex:0];
        // NSNumberしかNSDictionaryに入れられないので、NSNumberにする
        NSNumber *score = [NSNumber numberWithInt:[scoreString intValue]];
        
        // NSDictionaryをキーscoreに対してスコアを、shogoにたいして称号の値を入れたものを作り、それをshogoArrayに追加する。
        [self.shogoAry addObject:[NSDictionary dictionaryWithObjectsAndKeys:score ,@"score",[lineComponent objectAtIndex:1],@"shogo", nil]];
    }
}

+ (NSString *)getShogoForScore{
    NSArray *shogoAry = [GameModel sharedGameModel].shogoAry;
    NSDictionary *shogoDict;
    if (SCORE_SORT_TYPE == @"ASC"){
        NSArray *shogo = [shogoAry filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"score => %lld",[GameModel getScore]]];
        if (shogo.count == 0)
            return [GameModel getShogoForDefault];
        shogoDict = [shogo objectAtIndex:0];
    }else {
        NSArray *shogo = [shogoAry filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"score <= %lld",[GameModel getScore]]];
        if (shogo.count == 0)
            return [GameModel getShogoForDefault];
        shogoDict = [shogo lastObject];
    }
    return [shogoDict valueForKey:@"shogo"];
}

+ (NSString *)getShogoForHighScore{
    NSArray *shogoAry = [GameModel sharedGameModel].shogoAry;
    NSDictionary *shogoDict;
    if (SCORE_SORT_TYPE == @"ASC"){
        NSArray *shogo = [shogoAry filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"score => %lld",[GameModel getHighScore]]];
        if (shogo.count == 0)
            return [GameModel getShogoForDefault];
        shogoDict = [shogo objectAtIndex:0];
    }else {
        NSArray *shogo = [shogoAry filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"score <= %lld",[GameModel getHighScore]]];
        if (shogo.count == 0)
            return [GameModel getShogoForDefault];
        shogoDict = [shogo lastObject];
    }
    NSString *shogoString = [shogoDict valueForKey:@"shogo"];
    return shogoString;
}

+ (NSString *)getShogoForDefault{
    if (SCORE_SORT_TYPE == @"ASC")
        return [[[GameModel sharedGameModel].shogoAry lastObject] valueForKey:@"shogo"];
    else 
        return [[[GameModel sharedGameModel].shogoAry objectAtIndex:0] valueForKey:@"shogo"];
}

#pragma mark Life Cycle
-(void)dealloc{
    [instanceOfGameModel release];
    instanceOfGameModel = nil;
    [self.dict removeAllObjects];
    [_dict release];
    [_shogoAry removeAllObjects];
    [_shogoAry release];
    [super dealloc];
}

- (id)init{
    if ((self = [super init])) {
        // 道具箱の初期化
        self.dict = [[[NSMutableDictionary alloc] init] autorelease];
        [self setShogo];
        
    }
    return self;
}

@end
