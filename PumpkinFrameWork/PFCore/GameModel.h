//
//  GameModel.h
//  PFSample
//
//  Created by Kasajima Yasuo on 12/03/03.
//  Copyright (c) 2012年 kyoto. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PFObject.h"

#define HIGH_SCORE_KEY @"HighScore"
#define SCORE_KEY @"Score"
#define TIME_KEY @"Time"
#define PLAY_COUNT_KEY @"PlayCount"
#define BEFORE_VIEW_KEY @"BeforeView"

@interface GameModel : PFObject
@property (nonatomic, retain)NSMutableDictionary *dict;
@property (nonatomic, retain)NSMutableArray *shogoAry;

+ (GameModel*)sharedGameModel;
#pragma mark savedDict
// savedDict
+ (void)saveValue:(id)value withKey:(NSString*)key;
+ (void)saveInteger:(int)value withKey:(NSString*)key;
+ (void)saveDouble:(double)value withKey:(NSString*)key;
+ (void)saveLongLong:(long long int)value withKey:(NSString*)key;
+ (void)saveImage:(UIImage *)image withKey:(NSString*)key;
+ (id)savedValueForKey:(NSString*)key;
+ (int)savedIntegerForKey:(NSString*)key;
+ (float)savedDoubleForKey:(NSString*)key;
+ (long long)savedLongLongForKey:(NSString*)key;
+ (UIImage*)savedImageForKey:(NSString*)key;
// dict
+ (void)setValue:(id)value withKey:(NSString*)key;
+ (void)setInteger:(int)value withKey:(NSString*)key;
+ (void)setDouble:(double)value withKey:(NSString*)key;
+ (void)setLongLong:(long long int)value withKey:(NSString*)key;
+ (id)getValueForKey:(NSString*)key;
+ (int)getIntegerForKey:(NSString*)key;
+ (float)getDoubleForKey:(NSString*)key;
+ (long long)getLongLongForKey:(NSString*)key;

#pragma mark Score
// スコア
+ (long long int)getHighScore;
+ (void)setHighScore:(long long int)score;
+ (long long int)getScore;
+ (void)setScore:(long long int)score;
+ (void)updateScore:(long long int)score;
// タイマー
+ (double)getTime;
+ (void)updateTime:(double)time;
+ (void)resetTime;
// 回数
+ (long long int)getPlayCount;
+ (void)updatePlayCount;
// 称号
+ (NSString *)getShogoForScore;
+ (NSString *)getShogoForHighScore;
+ (NSString *)getShogoForDefault;
@end
