//
//  IndicatorController.m
//  Hengao
//
//  Created by Kasajima Yasuo on 11/12/29.
//  Copyright (c) 2011年 kyoto. All rights reserved.
//

#import "IndicatorController.h"

@implementation IndicatorController

@synthesize view = _view;
@synthesize indicator = _indicator;

static IndicatorController *instanceOfIndicatorController;

+ (IndicatorController*)sharedController{
    if (!instanceOfIndicatorController) {
        instanceOfIndicatorController = [[self alloc] init];
    }
    return instanceOfIndicatorController;
}

+ (void)startIndicator{
    NSLog(@"start");
    [[UIApplication sharedApplication].keyWindow addSubview:[IndicatorController sharedController].view];
    [[IndicatorController sharedController].indicator startAnimating];
}

+ (void)stopIndicator{
    NSLog(@"end");
    [[IndicatorController sharedController].view removeFromSuperview];
    [[IndicatorController sharedController].indicator stopAnimating];
}

- (id)init{
    self = [super init];
    if (self) {
        CGSize winSize = [UIScreen mainScreen].bounds.size;
        
        //view  = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 480)];
        _view = [[UIView alloc] initWithFrame:CGRectMake(winSize.width/2 - 50, winSize.height/2 - 50, 100, 100)];
        _view.backgroundColor = [UIColor colorWithWhite:0 alpha:0.5];
        
        _indicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
        //indicator.frame = CGRectMake(winSize.width/2 - 25, winSize.height/2 - 25, 50, 50);
        _indicator.frame = CGRectMake(25, 25, 50, 50);
        [_view addSubview:_indicator];
    }
    return self;
}

- (void)startIndicator{
    NSLog(@"start");
    [[UIApplication sharedApplication].keyWindow addSubview:_view];
    [_indicator startAnimating];
}

- (void)stopIndicator{
    NSLog(@"end");
    [_view removeFromSuperview];
    [_indicator stopAnimating];
}

- (void)dealloc{
    [super dealloc];
    [_view release];
    [_indicator release];
    [instanceOfIndicatorController release];
    instanceOfIndicatorController = nil;
}

@end
