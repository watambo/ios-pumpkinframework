//
//  IndicatorController.h
//  Hengao
//
//  Created by Kasajima Yasuo on 11/12/29.
//  Copyright (c) 2011年 kyoto. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface IndicatorController : NSObject
@property(nonatomic, retain)UIView *view;
@property(nonatomic, retain)UIActivityIndicatorView *indicator;

+ (IndicatorController*)sharedController;
+ (void)startIndicator;
+ (void)stopIndicator;
- (void)startIndicator;
- (void)stopIndicator;

@end
