//
//  GameKitController.m
//  GameCenterSample
//
//  Created by Kasajima Yasuo on 12/02/16.
//  Copyright (c) 2012年 kyoto. All rights reserved.
//


#import "GameKitController.h"

@implementation GameKitController

@synthesize playerName = _playerName;
@synthesize delegate = _delegate;
@synthesize matchedPlayers = _matchedPlayers;

static GameKitController *instanceOfGameKitController = nil;

+(GameKitController*)sharedGameKitController{
    if (!instanceOfGameKitController) {
        instanceOfGameKitController = [[self alloc] init];
    }
    return instanceOfGameKitController;
}

- (void)loginToGameCenter{
    loginCount ++;
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    [ud setInteger:loginCount forKey:@"GameCenterKitLoginCount"];
    [ud synchronize];
    GameKitHelper *gkHelper = [GameKitHelper sharedGameKitHelper];
    gkHelper.delegate = self;
    [gkHelper authenticateLocalPlayer];
}

-(id)init{
    if((self = [super init])){
        NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];                      
        loginCount = [ud integerForKey:@"GameCenterKitLoginCount"];
        [self loginToGameCenter];
        
        _matchedPlayers = [[NSMutableDictionary alloc] init];
    }
    return self;
}

#pragma mark 表示系
- (void)showLeaderboard{
    GKLocalPlayer *localPlayer = [GKLocalPlayer localPlayer];
    if(localPlayer.authenticated){
        GameKitHelper *gkHelper = [GameKitHelper sharedGameKitHelper];
        [gkHelper showLeaderboard];
    }else{
        if (loginCount<=3) 
            [self loginToGameCenter];
        else
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"gamecenter:"]];
    }
}

- (void)showAchievements{
    GKLocalPlayer *localPlayer = [GKLocalPlayer localPlayer];
    if(localPlayer.authenticated){
        GameKitHelper *gkHelper = [GameKitHelper sharedGameKitHelper];
        [gkHelper showAchievements];
    }else{
        if (loginCount<=3) 
            [self loginToGameCenter];
        else
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"gamecenter:"]];
    }
}

#pragma mark スコアの送信
-(void)submitScoreWithScore:(long long int)score category:(NSString*)category{
    GKLocalPlayer *localPlayer = [GKLocalPlayer localPlayer];
    if(localPlayer.authenticated){
        GameKitHelper *gkHelper = [GameKitHelper sharedGameKitHelper];
        [gkHelper submitScore:score category:category];
    }else{
        
    }
}

-(void)submitScoreWithScore:(long long int)score categoryNum:(int)categoryNum{
    [self submitScoreWithScore:score category:leaderboardID[categoryNum]];
}

#pragma mark アチーブメント
- (void)reportAchievementWithPercent:(float)percent achievementID:(NSString*)achievementID{
    GKLocalPlayer *localPlayer = [GKLocalPlayer localPlayer];
    if(localPlayer.authenticated){
        GameKitHelper *gkHelper = [GameKitHelper sharedGameKitHelper];
        [gkHelper reportAchievementWithID:achievementID percentComplete:percent];
    }else{
        
    }
}

- (void)reportAchievementWithPercent:(float)percent achievementNum:(int)achievementNum{
    [self reportAchievementWithPercent:percent achievementID:achievementID[achievementNum]];
}

// 前のバージョンを残すため
- (void)reportAchievementWithID:(NSString*)achievementId percentComplete:(float)percent{
    [self reportAchievementWithPercent:percent achievementID:achievementId];
}


#pragma mark --
#pragma mark マッチ系

#pragma mark マッチのサイクル
// リクエストを作る
-(void) setMatachingRequestWithMinPlayers:(int)min maxPlayers:(int)max{
    GKLocalPlayer *localPlayer = [GKLocalPlayer localPlayer];
    if(localPlayer.authenticated){
        GKMatchRequest *request = [[[GKMatchRequest alloc] init] autorelease];
        request.minPlayers = min;
        request.maxPlayers = max;
        
        GameKitHelper *gkHelper = [GameKitHelper sharedGameKitHelper];
        [gkHelper showMatchmakerWithRequest:request];
    }
}

-(void) dissconnectCurrentMatch{
    [[GameKitHelper sharedGameKitHelper] disconnectCurrentMatch];
}


#pragma mark マッチのデータ送信
-(void) sendDataWithDict:(NSDictionary*)dic{
    NSData *data= [NSKeyedArchiver archivedDataWithRootObject:dic];
    [[GameKitHelper sharedGameKitHelper] sendDataToAllPlayers:data];
}




#pragma mark - GameKitHelper Protocol
#pragma mark Player関係
-(void) onLocalPlayerAuthenticationChanged{
    // ログインされた
    GKLocalPlayer *localPlayer = [GKLocalPlayer localPlayer];
    if(localPlayer.authenticated){
        NSLog(@"%@",NSStringFromSelector(_cmd));
        //[self setMatckRequest];
        self.playerName = localPlayer.alias;
    }
}
-(void) onFriendListReceived:(NSArray*)friends{
    
}
-(void) onPlayerInfoReceived:(NSArray*)players{
    
}
#pragma mark Score関係
-(void) onScoresSubmitted:(bool)success{
    if (success) {
        NSLog(@"%@",NSStringFromSelector(_cmd));
    }
}
-(void) onScoresReceived:(NSArray*)scores{
    NSLog(@"%@",scores);
}
#pragma mark Achievement関係
-(void) onAchievementReported:(GKAchievement*)achievement{
    NSLog(@"%@",NSStringFromSelector(_cmd));
}
-(void) onAchievementsLoaded:(NSDictionary*)achievements{
    NSLog(@"%@",NSStringFromSelector(_cmd));
}
-(void) onResetAchievements:(bool)success{
    if (success) {
        NSLog(@"%@",NSStringFromSelector(_cmd));
    }
}
#pragma mark Match関係
-(void) onMatchFound:(GKMatch*)match{
    NSLog(@"%@ %@",NSStringFromSelector(_cmd),match);
}
-(void) onPlayersAddedToMatch:(bool)success{
    NSLog(@"%@",NSStringFromSelector(_cmd));
}
-(void) onReceivedMatchmakingActivity:(NSInteger)activity{
    NSLog(@"%@",NSStringFromSelector(_cmd));
}

// 繋がったユーザー。PlayerIDとaliasを結びつけておく
-(void) onPlayerConnected:(NSString*)playerID{
    NSLog(@"%@ %@",NSStringFromSelector(_cmd),playerID);
    
    NSArray *identifers = [NSArray arrayWithObjects:playerID, nil];
    [GKPlayer loadPlayersForIdentifiers:identifers withCompletionHandler:^(NSArray *players,NSError *error){
        if (error != nil) {
            
        }
        if (players != nil) {
            GKPlayer *player = [players objectAtIndex:0];
            NSLog(@"%@",player.alias);
            [_matchedPlayers setObject:player.alias forKey:playerID];
            [_delegate recivedPlayerNames:_matchedPlayers];
        }
    
    }];
}

-(void) onPlayerDisconnected:(NSString*)playerID{
    [_delegate onPlayerDisconnected:playerID];
    NSLog(@"%@ %@",NSStringFromSelector(_cmd),playerID);
    
    [_matchedPlayers removeObjectForKey:playerID];
    if (_matchedPlayers.count == 0) {
        [_delegate onGameEnd];
    }
}

// すべてのプライヤーが準備可能となった時
-(void) onStartMatch{
    NSLog(@"%@",NSStringFromSelector(_cmd));
    [_delegate onGameStart];
}

// データを受信した時
-(void) onReceivedData:(NSData*)data fromPlayer:(NSString*)playerID{
    NSDictionary *dict = (NSDictionary*)[NSKeyedUnarchiver unarchiveObjectWithData:data];    
    [_delegate recivedDict:dict fromPlayer:playerID];
}

// 接続が切れた時
-(void) onDisconnectMatch{
    self.matchedPlayers = [NSMutableDictionary dictionary];
    NSLog(@"%@",NSStringFromSelector(_cmd));
}

#pragma mark 表示関係
-(void) onMatchmakingViewDismissed{
    
}
-(void) onMatchmakingViewError{
    
}
-(void) onLeaderboardViewDismissed{
    
}
-(void) onAchievementsViewDismissed{
    
}


@end
