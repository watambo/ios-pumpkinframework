//
//  GameKitController.h
//  GameCenterSample
//
//  Created by Kasajima Yasuo on 12/02/16.
//  Copyright (c) 2012年 kyoto. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GameKitHelper.h"
#import "Parameter.h"

@protocol GameKitControllerProtocol
// データを取得
-(void)recivedPlayerNames:(NSDictionary*)mathcedPlayers;
-(void)recivedDict:(NSDictionary *)dict fromPlayer:(NSString*)playerID;
// ゲームサイクル
- (void) onGameStart;
- (void) onGameEnd;

// プレイヤーの接続が切れた
- (void) onPlayerDisconnected:(NSString*)playerID;

@end

@interface GameKitController : NSObject<GameKitHelperProtocol>{
    int loginCount;
}
@property(nonatomic, retain)NSString *playerName;
@property(nonatomic, retain)id <GameKitControllerProtocol>delegate;
@property(nonatomic, retain)NSMutableDictionary *matchedPlayers;

+ (GameKitController*)sharedGameKitController;
#pragma mark --
#pragma mark リーダーボードとアチーブメント
#pragma mark 表示系
- (void)showLeaderboard;
- (void)showAchievements;
#pragma  mark データ送信系
- (void)submitScoreWithScore:(long long int)score category:(NSString*)category;
- (void)submitScoreWithScore:(long long int)score categoryNum:(int)categoryNum;
- (void)reportAchievementWithID:(NSString*)achievementId percentComplete:(float)percent;
- (void)reportAchievementWithPercent:(float)percent achievementNum:(int)achievementNum;
- (void)reportAchievementWithPercent:(float)percent achievementID:(NSString*)achievementID;

#pragma mark --
#pragma mark マッチング
#pragma mark マッチのサイクル
-(void) setMatachingRequestWithMinPlayers:(int)min maxPlayers:(int)max; //matchging画面を表示したい時。
-(void) dissconnectCurrentMatch;
#pragma mark マッチのデータ送信
-(void) sendDataWithDict:(NSDictionary*)dic;
@end
