//
//  GameController.m
//  PFSample
//
//  Created by Kasajima Yasuo on 12/03/05.
//  Copyright (c) 2012年 kyoto. All rights reserved.
//

#import "GameController.h"
#import "ViewController.h"
#import "GameModel.h"
#import "Appirater.h"

@implementation GameController

@synthesize viewController = _viewController;
@synthesize BGMPlayer = _BGMPlayer;

static GameController *instanceOfGameController = nil;

#pragma mark Update Method
- (void)updateTimer:(NSTimer*)timer{
    [GameModel updateTime:timer.timeInterval];
}

#pragma mark Game Cycle
+ (void)gameStart{
    [GameModel sharedGameModel].dict = [[[NSMutableDictionary alloc] init] autorelease];
    
    [[GameController sharedGameController].viewController gameStart];
    
}

+ (void)timerStart{
    [[GameController sharedGameController] startTimer:@selector(updateTimer:)];
}

+ (void)timerStop{
    [[GameController sharedGameController] stopTimer:@selector(updateTimer:)];
    [GameModel resetTime];
}

+ (void)gameEnd{
    // 時間を止める
    [GameController timerStop];
    
    // スコアをハイスコアにセットする
    [GameModel setHighScore:[GameModel getScore]];
    // 画面遷移
    [[GameController sharedGameController].viewController gameEnd];
    
    // Review
    [Appirater userDidSignificantEvent:YES];
}

// ResultViewからStart画面へ
+ (void)gameReStart{
    [[GameController sharedGameController].viewController gameRestart];
}

// GameViewからStartView
+ (void)gameRemove{
    // 時間を止める
    [GameController timerStop];
    
    // 移動
    [[GameController sharedGameController].viewController gameRemove];
    
}

// GameViewからGameView
+ (void)gameReset{
    // 時間を止める
    [GameController timerStop];
    
    // リセット
    [GameModel sharedGameModel].dict = [[[NSMutableDictionary alloc] init] autorelease];
    
    // 移動
    [[GameController sharedGameController].viewController gameReset];
    
}

+ (void)gameRetry{
    // リセット
    [GameModel sharedGameModel].dict = [[[NSMutableDictionary alloc] init] autorelease];
    
    // 移動
    [[GameController sharedGameController].viewController gameRetry];
}

#pragma mark GlobalMethod
+ (NSString *)getLang{
    NSArray *languages = [NSLocale preferredLanguages];
    NSString *lang = [languages objectAtIndex:0];
    return lang;
}

+ (BOOL)isJapan{
    return [[GameController getLang] isEqualToString:@"ja"];
}

+ (BOOL)isChina{
    return [[GameController getLang] isEqualToString:@"cn"];
}

+ (UIImage *) captureScreen {
    UIView *view = [GameController sharedGameController].viewController.view;
    UIGraphicsBeginImageContext(view.bounds.size);
    [view.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    // 変更するサイズ
    CGSize size = CGSizeMake(image.size.width * 2, image.size.height * 2);
    
    UIImage *resultImage;
    UIGraphicsBeginImageContext(size);
    [image drawInRect:CGRectMake(0, 0, size.width, size.height)];
    resultImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return resultImage;
}
#pragma mark AudioMethod
+ (void)playAudioFileName:(NSString *)name ofType:(NSString *)type{
    NSString *path = [[NSBundle mainBundle] pathForResource:name ofType:type];  
    NSURL *url = [NSURL fileURLWithPath:path];
    AVAudioPlayer *audio = [[AVAudioPlayer alloc] initWithContentsOfURL:url error:nil];
    audio.delegate = [GameController sharedGameController];
    [audio play];
}

+ (void)playBGMFileName:(NSString *)name ofType:(NSString *)type{
    NSString *path = [[NSBundle mainBundle] pathForResource:name ofType:type];  
    NSURL *url = [NSURL fileURLWithPath:path];
    [GameController sharedGameController].BGMPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:url error:nil];
    [GameController sharedGameController].BGMPlayer.delegate = [GameController sharedGameController];
    [GameController sharedGameController].BGMPlayer.numberOfLoops = -1;
    [[GameController sharedGameController].BGMPlayer play];
}

+ (void)stopBGM{
    [[GameController sharedGameController].BGMPlayer stop];
}

- (void)audioPlayerDecodeErrorDidOccur:(AVAudioPlayer *)player error:(NSError *)error{
    [player release];
}

-(void)audioPlayerDidFinishPlaying:(AVAudioPlayer *)player successfully:(BOOL)flag{
    [player release];
}

#pragma  mark LifeCycle
+(GameController*)sharedGameController{
    if (!instanceOfGameController) {
        instanceOfGameController = [[GameController alloc] init];
    }
    return instanceOfGameController;
}

-(id)init{
    if ((self = [super init])) {
        NSString *path = [[NSBundle mainBundle] pathForResource:@"PushBtn" ofType:@"wav"];  
        NSURL *url = [NSURL fileURLWithPath:path]; 
        AVAudioPlayer *audio = [[AVAudioPlayer alloc] initWithContentsOfURL:url error:nil];
        audio.delegate = self;
        audio.currentTime = 0;
        audio.volume = 0;
        [audio play];
        [audio release];
    }
    return self;
}

-(void)dealloc{
    [instanceOfGameController release];
    instanceOfGameController = nil;
    [_viewController release];
    [_BGMPlayer release];
    [super dealloc];
}

@end
