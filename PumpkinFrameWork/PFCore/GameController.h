//
//  GameController.h
//  PFSample
//
//  Created by Kasajima Yasuo on 12/03/05.
//  Copyright (c) 2012年 kyoto. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PFObject.h"
#import <AVFoundation/AVFoundation.h>

@class ViewController;

@interface GameController : PFObject<AVAudioPlayerDelegate>
@property(nonatomic, retain)ViewController *viewController;
@property(nonatomic, retain)AVAudioPlayer *BGMPlayer;
#pragma mark Game Cycle
+ (void)gameStart;
+ (void)timerStart;
+ (void)timerStop;
+ (void)gameEnd;
+ (void)gameReStart;
+ (void)gameRemove;
+ (void)gameReset;
+ (void)gameRetry;
#pragma mark GlobalMetho
+ (NSString *)getLang;
+ (BOOL)isJapan;
+ (BOOL)isChina;
+ (UIImage *) captureScreen;
#pragma mark AudioMethod
+ (void)playAudioFileName:(NSString*)name ofType:(NSString *)type;
+ (void)playBGMFileName:(NSString *)name ofType:(NSString *)type;
+ (void)stopBGM;
#pragma mark LifeCycle
+(GameController*)sharedGameController;
@end
