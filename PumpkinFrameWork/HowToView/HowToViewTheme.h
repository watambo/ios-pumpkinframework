//
//  HowToView.h
//  LittleGameSample
//
//  Created by Kasajima Yasuo on 12/03/13.
//  Copyright (c) 2012年 kyoto. All rights reserved.
//

#import "PFGameSubView.h"

@interface HowToViewTheme : PFGameSubView<UIScrollViewDelegate>
@property int pageNum;
@property(nonatomic, retain)UIButton *goBackBtn;
@property(nonatomic, retain)UIButton *goFowardBtn;
@property(nonatomic, retain)UIScrollView *scrollView;
@end
