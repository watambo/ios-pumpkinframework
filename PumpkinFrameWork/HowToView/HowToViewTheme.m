//
//  HowToView.m
//  LittleGameSample
//
//  Created by Kasajima Yasuo on 12/03/13.
//  Copyright (c) 2012年 kyoto. All rights reserved.
//

#import "HowToViewTheme.h"

@implementation HowToViewTheme

@synthesize pageNum = _pageNum;
@synthesize goBackBtn = _goBackBtn;
@synthesize goFowardBtn = _goFowardBtn;
@synthesize scrollView = _scrollView;

#pragma mark pressBtn
- (void)pressGoBackBtn:(id)sender{
    if(floor(_scrollView.contentOffset.x/self.bounds.size.width)==_scrollView.contentOffset.x/self.bounds.size.width){
        CGRect frame = _scrollView.frame; 
        frame.origin.x = _scrollView.contentOffset.x - self.bounds.size.width;
        frame.origin.y = 0;
        [_scrollView scrollRectToVisible:frame animated:YES];
    }
}

- (void)pressGoFowardBtn:(id)sender{
    if(floor(_scrollView.contentOffset.x/self.bounds.size.width)==_scrollView.contentOffset.x/self.bounds.size.width){
        CGRect frame = _scrollView.frame; 
        frame.origin.x = _scrollView.contentOffset.x + self.bounds.size.width;
        frame.origin.y = 0;
        [_scrollView scrollRectToVisible:frame animated:YES];
    }
}

#pragma mark grayscaleTheme
- (void)grayScaleTheme{
    _scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, self.bounds.size.width, self.bounds.size.height)];
    _scrollView.pagingEnabled = YES;  
    _scrollView.contentSize = CGSizeMake(_scrollView.frame.size.width*_pageNum, _scrollView.frame.size.height);
    _scrollView.showsHorizontalScrollIndicator = NO;  
    _scrollView.showsVerticalScrollIndicator = NO;  
    [self addSubview:_scrollView];
    _scrollView.delegate = self;
    
    for (int i = 0; i<_pageNum; i++) {
        UIImageView *imageView = [UIImageView imageViewWithFileName:[NSString stringWithFormat:@"HowToImg_%d",i]];
        [imageView setPosition:CGPointMake(self.bounds.size.width*i, 0)];
        [_scrollView addSubview:imageView];
    }
    
    [self installColoredButtonNamed:@"ClosedBtn" inPosition:CGPointMake(10+25, 380)];
    if (_pageNum>1) {
        self.goBackBtn = [self installColoredButtonNamed:@"GoBackBtn" inPosition:CGPointMake(10+100+25, 380)];
        _goBackBtn.enabled = NO;
        self.goFowardBtn =  [self installColoredButtonNamed:@"GoFowardBtn" inPosition:CGPointMake(10+100*2+25, 380)];
    }
}

//スクロールされたときに呼ばれる。
- (void)scrollViewDidScroll:(UIScrollView *)sender{
    if (_scrollView.contentOffset.x<=0) {
        _goBackBtn.enabled = FALSE;
    }else if(_scrollView.contentOffset.x >= (_pageNum-1)*self.bounds.size.width){
        _goFowardBtn.enabled = FALSE;
    }else{
        _goBackBtn.enabled= TRUE;
        _goFowardBtn.enabled = TRUE;
    }
}




- (void)useTheme:(enum ThemeType)type{
    switch (type) {
        case Default:
            
            break;
        case GrayScale:
            [self grayScaleTheme];
            break;
        default:
            break;
    }
}

#pragma mark LifeCycle
- (void)dealloc{
    [_goBackBtn release];
    [_goFowardBtn release];
    [super dealloc];
}
@end
