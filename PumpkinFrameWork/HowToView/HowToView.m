//
//  HowToView.m
//  LittleGameSample
//
//  Created by Kasajima Yasuo on 12/03/14.
//  Copyright (c) 2012年 kyoto. All rights reserved.
//

#import "HowToView.h"

@implementation HowToView

#pragma mark LifeCycle
- (void)startView{
    // HowToViewが現れるときの遷移
    [self transitionFadeIn:^{}];
}

- (void)createView{
    // ページの数を設定する。
    self.pageNum = 1;
    // テーマを使うときは指定
    [self useTheme:GrayScale];
    // テーマを使わないときは上の1行をコメントアウトし以下にコードを書く
}

#pragma mark press btn
- (void)pressClosedBtn:(id)sender{
    // Colsedボタンを押したときの遷移
    [self removeFromSuperviewWithTransitionFadeOut:^{}];
}
@end