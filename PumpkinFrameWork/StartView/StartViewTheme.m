//
//  StartViewTheme.m
//  LittleGameSample
//
//  Created by Kasajima Yasuo on 12/03/13.
//  Copyright (c) 2012年 kyoto. All rights reserved.
//

#import "StartViewTheme.h"
#import "Parameter.h"

@implementation StartViewTheme

- (void)defaultsTheme{
    // HowToBtn
    [self installButtonNamed:@"HowToBtn" inPosition:CGPointMake(10,0)];
    
    // Review
    [self installButtonNamed:@"ReviewBtn" inPosition:CGPointMake(65,0)];
    
    // ShareBtn
    [self installButtonNamed:@"ShareBtn" inPosition:CGPointMake(235,0)];
    
    // Leaderboard
    [self installButtonNamed:@"LeaderboardBtn" inPosition:CGPointMake(60,310)];
    
    // gameStartボタン
    [self installButtonNamed:@"GameStartBtn" inPosition:CGPointMake(60,250)];
    
    // MoreApp
    [self installButtonNamed:@"MoreAppBtn" inPosition:CGPointMake(60,370)];
    
    
    // タイトル
    [self installImageViewWithFileName:@"TitleImg" inPosition:CGPointMake(0, 50)];
    
    // ハイスコア
    NSString *labelString = [NSString stringWithFormat:NSLocalizedString(@"HighScore  %lld　%@", nil),[GameModel getHighScore],SCORE_COUNT_NAME];
    [self installLabelWithText:labelString atCenter:CGPointMake(self.bounds.size.width * 0.5, 100 + 120) size:15];
    // 称号
    NSString *shogoHighScore = [GameModel getShogoForHighScore];
    shogoHighScore = [NSString stringWithFormat:NSLocalizedString(@"Title %@", nil),shogoHighScore];
    [self installLabelWithText:shogoHighScore atCenter:CGPointMake(self.bounds.size.width * 0.5, 120 + 120) size:15];
    

    // 保存した称号
    NSString *savedShogo = [GameModel savedValueForKey:@"shogo"];
    if (!savedShogo) {
        // 保存しているのがなかったらデフォルトの称号
        savedShogo = [GameModel getShogoForDefault];
    }
    UILabel *savedShogoLabel = [UILabel labelWithText:savedShogo];
    [savedShogoLabel setPosition:CGPointMake(0, 0)];
    //[self addSubview:savedShogoLabel];
}

#pragma mark GrayScale
- (void)grayScaleTheme{
    CGSize winSize = self.bounds.size;
    // 背景画像
    UIImageView *backgroundImgView = [UIImageView imageViewWithFileName:@"StartViewBgImg" withColor:THEME_COLOR];
    [self addSubview:backgroundImgView];
    
    
    UIImageView *titleImgView = [UIImageView imageViewWithFileName:@"TitleImg"];
    [titleImgView setPosition:CGPointMake(0, 80)];
    [self addSubview:titleImgView];
    
    
    /* 上のボタン */
    int buttonNum = kPFHowToFlag ? 5 : 4;
    int headerBtnHeight = 10; // ボタンの高さ
    int marginWidth = 10;
    int buttonSize = 50;
    int buttonMargin = (winSize.width - marginWidth*2 - buttonSize*buttonNum)/(buttonNum*2);
    int buttonShowNum = 0;
    if (kPFHowToFlag) {
        // HowToBtn
        [self installColoredButtonNamed:@"HowToBtn" 
                             inPosition:CGPointMake(marginWidth+buttonMargin*(1+buttonShowNum*2)+buttonSize*buttonShowNum, headerBtnHeight)];
        buttonShowNum++;
    }
    
    
    // Review
    [self installColoredButtonNamed:@"ReviewBtn" 
                         inPosition:CGPointMake(marginWidth+buttonMargin*(1+buttonShowNum*2)+buttonSize*buttonShowNum, headerBtnHeight)
     ];
    buttonShowNum++;
    
    // Twitter
    [self installColoredButtonNamed:@"TweetBtn" 
                         inPosition:CGPointMake(marginWidth+buttonMargin*(1+buttonShowNum*2)+buttonSize*buttonShowNum, headerBtnHeight)];
    buttonShowNum++;
    
    // ShareBtn
    [self installColoredButtonNamed:@"ShareBtn" inPosition:CGPointMake(marginWidth+buttonMargin*(1+buttonShowNum*2)+buttonSize*buttonShowNum, headerBtnHeight)];
    buttonShowNum++;
    
    // Leaderboard
    [self installColoredButtonNamed:@"LeaderboardBtn" inPosition:CGPointMake(marginWidth+buttonMargin*(1+buttonShowNum*2)+buttonSize*buttonShowNum, headerBtnHeight)];
    
    /* 下のボタン */
    // gameStartボタン
    [self installColoredButtonNamed:@"GameStartBtn" inPosition:CGPointMake(60, 320)];
    
    // MoreApp
    [self installColoredButtonNamed:@"MoreAppBtn" inPosition:CGPointMake(60, 380)];
    
    // ハイスコア
    NSString *labelString = [NSString stringWithFormat:NSLocalizedString(@"HighScore  %lld　%@", nil),[GameModel getHighScore],SCORE_COUNT_NAME];
    UILabel *label = [UILabel labelWithText:labelString];
    label.frame = CGRectMake(0, 290, 320, 20);
    label.textAlignment = UITextAlignmentCenter;
    label.textColor = [UIColor grayColor];
    [self addSubview:label];
}

- (void)useTheme:(enum ThemeType)type{
    switch (type) {
        case Default:
            [self defaultsTheme];
            break;
        case GrayScale:
            [self grayScaleTheme];
        default:
            break;
    }
}


@end
