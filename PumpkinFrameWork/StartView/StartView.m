//
//  StartView.m
//  PFSample
//
//  Created by Kasajima Yasuo on 12/03/03.
//  Copyright (c) 2012年 kyoto. All rights reserved.
//

#import "StartView.h"
#import "PFHeader.h"
#import "SocialWebView.h"
#import "GameKitController.h"
#import "Appirater.h"
#import "HowToView.h"
#import "PFWebView.h"

@implementation StartView

#pragma mark Life Cycle
- (void)startView{
    // viewが表示されてから最初に何かしたい時。ただし、起動時には呼び出されず、ほかの画面からの遷移のみ呼び出される。
    // アニメーションとかしたい場合はここ
    [self transitionMoveFromRight:^{}];
}

- (void)createView{
    // テーマを使う
    [self useTheme:GrayScale];
    // テーマを使わない場合は上の行をコメントアウトして、この下にコードを書く
}



// これより下は基本的には書き換えない
#pragma mark SocialWeb
- (void)setSocialWebWithType:(enum SocialType)type{
    SocialWebView *webView = [SocialWebView socialWebViewWithType:type];
    
    // このstatusがtweetされる内容
    NSString *status = [NSString stringWithFormat:NSLocalizedString(@"My HighScore is %lld", nil),[GameModel getHighScore]];
    [webView setStatus:status];
    [self addSubview:webView];
}

#pragma mark press Btn
// press button
- (void)pressGameStartBtn:(id)sender{
    [GameController playAudioFileName:@"PushBtn" ofType:@"wav"];
    [GameController gameStart];
}
- (void)pressTweetBtn:(id)sender{
    [GameController playAudioFileName:@"PushBtn" ofType:@"wav"];
    [self setSocialWebWithType:Twitter];
}

- (void)pressMixiBtn:(id)sender{
    [GameController playAudioFileName:@"PushBtn" ofType:@"wav"];
    [self setSocialWebWithType:Mixi];
}

- (void)pressFacebookBtn:(id)sender{
    [GameController playAudioFileName:@"PushBtn" ofType:@"wav"];
    [self setSocialWebWithType:Facebook];
}

- (void)pressAmebaBtn:(id)sender{
    [GameController playAudioFileName:@"PushBtn" ofType:@"wav"];
    [self setSocialWebWithType:Ameba];
}

- (void)pressLeaderboardBtn:(id)sender{
    [GameController playAudioFileName:@"PushBtn" ofType:@"wav"];
    [[GameKitController sharedGameKitController] showLeaderboard];
}

- (void)pressReviewBtn:(id)sender{
    [GameController playAudioFileName:@"PushBtn" ofType:@"wav"];
    [Appirater rateApp];
}

- (void)pressMoreAppBtn:(id)sender{
    if ([GameController isJapan]) {
        PFWebView *webView = [PFWebView view];
        [webView useTheme:GrayScale];
        [self addSubview:webView];
        [webView loadURLString:MORE_APP_URL];
    }else{
        NSURL *url = [NSURL URLWithString:MORE_APP_URL];
        [[UIApplication sharedApplication] openURL:url];
    }
}

- (void)pressHowToBtn:(id)sender{
    [GameController playAudioFileName:@"PushBtn" ofType:@"wav"];
    [GameModel setValue:[GameController captureScreen] withKey:BEFORE_VIEW_KEY];
    HowToView *view = [HowToView view];
    [self addSubview:view];
    [view startView];
}

- (void)pressShareBtn:(id)sender{
    [GameController playAudioFileName:@"PushBtn" ofType:@"wav"];
    if ([GameController isJapan]) {
        UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:NSLocalizedString(@"Share Your HighScore" ,nil)
                                                                 delegate:self 
                                                        cancelButtonTitle:@"Cancel" 
                                                   destructiveButtonTitle:nil 
                                                        otherButtonTitles:@"Twitter",@"Facebook",@"Mixiボイス",@"Amebaなう", nil];
        
        [actionSheet showInView:self];
        [actionSheet release];
    }else{
        UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:NSLocalizedString(@"Share Your HighScore" ,nil)
                                                                 delegate:self 
                                                        cancelButtonTitle:@"Cancel" 
                                                   destructiveButtonTitle:nil 
                                                        otherButtonTitles:@"Twitter",@"Facebook", nil];
        
        [actionSheet showInView:self];
        [actionSheet release];
    }
}

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    if ([GameController isJapan]) {
        switch (buttonIndex) {
            case 0:
                [self setSocialWebWithType:Twitter];
                break;
            case 1:
                [self setSocialWebWithType:Facebook];
                break;
            case 2:
                [self setSocialWebWithType:Mixi];
                break;
            case 3:
                [self setSocialWebWithType:Ameba];
                break;
            default:
                break;
        }
    }else{
        switch (buttonIndex) {
            case 0:
                [self setSocialWebWithType:Twitter];
                break;
            case 1:
                [self setSocialWebWithType:Facebook];
                break;
            default:
                break;
        }
    }
}

@end
