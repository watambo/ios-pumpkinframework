//
//  StartView.h
//  PFSample
//
//  Created by Kasajima Yasuo on 12/03/03.
//  Copyright (c) 2012年 kyoto. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "StartViewTheme.h"

@interface StartView : StartViewTheme<UIActionSheetDelegate>{
}
@end
