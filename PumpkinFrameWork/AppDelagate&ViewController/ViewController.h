//
//  ViewController.h
//  PFSample
//
//  Created by Kasajima Yasuo on 12/03/03.
//  Copyright (c) 2012年 kyoto. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>

@interface ViewController : UIViewController<AVAudioPlayerDelegate>
@property(nonatomic,retain)UIView *mainView;

- (void)gameStart;
- (void)gameEnd;
- (void)gameRestart;
- (void)gameRemove;
- (void)gameReset;
- (void)gameRetry;
@end
