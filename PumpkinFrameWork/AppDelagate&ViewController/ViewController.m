//
//  ViewController.m
//  PFSample
//
//  Created by Kasajima Yasuo on 12/03/03.
//  Copyright (c) 2012年 kyoto. All rights reserved.
//

#import "ViewController.h"
#import "PFHeader.h"
#import "StartView.h"
#import "GameView.h"
#import "ResultView.h"
#import "GameKitController.h"
#import <quartzcore/quartzcore.h>

@implementation ViewController

@synthesize mainView = _mainView;

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Release any cached data, images, etc that aren't in use.
}

#pragma remove Class view
- (UIImage *) captureScreen {
    UIGraphicsBeginImageContext(self.view.bounds.size);
    [self.view.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    // 変更するサイズ
    CGSize size = CGSizeMake(image.size.width * 2, image.size.height * 2);
    
    UIImage *resultImage;
    UIGraphicsBeginImageContext(size);
    [image drawInRect:CGRectMake(0, 0, size.width, size.height)];
    resultImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return resultImage;
}

- (void)removeViewWithClass:(Class)clase{
    // StartViewを剥がす
    NSArray *subviews = [self.mainView subviews];
    for (UIView *subview in subviews) {
        if (subview && [subview isKindOfClass:clase]) {
            [GameModel setValue:[self captureScreen] withKey:BEFORE_VIEW_KEY];
            [subview removeFromSuperview];
        }
    }
}



#pragma mark - View Transition
- (void)addSubViewWithClass:(Class)class{
    PFGameView *view = [class view];
    [self.mainView addSubview:view];
    [view startView];
}


- (void)gameStart{
    // StartViewを剥がす
    [self removeViewWithClass:[StartView class]];

    // GameViewを載せる
    [self addSubViewWithClass:[GameView class]];
}

- (void)gameEnd{
    // GameViewを剥がす
    [self removeViewWithClass:[GameView class]];
    
    // ResultViewを載せる
    [self addSubViewWithClass:[ResultView class]];
}

- (void)gameRestart{
    // ResultViewを剥がす
    [self removeViewWithClass:[ResultView class]];
    
    // StartViewを載せる
    [self addSubViewWithClass:[StartView class]];
}

- (void)gameRemove{
    // GameViewを剥がす
    [self removeViewWithClass:[GameView class]];
    
    // StartViewを載せる
    [self addSubViewWithClass:[StartView class]];
}

- (void)gameReset{
    // GameViewを剥がす
    [self removeViewWithClass:[GameView class]];
    
    // GameViewを載せる
    [self addSubViewWithClass:[GameView class]];
}

- (void)gameRetry{
    // ResultViewを剥がす
    [self removeViewWithClass:[ResultView class]];
    
    // GameViewを載せる
    [self addSubViewWithClass:[GameView class]];
}

#pragma mark - View lifecycle

-(void)dealloc{
    [_mainView release];
    [super dealloc];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    self.mainView = [PFGameView view];
    
    if ([kPFAdLocation isEqualToString:@"PortraitTop"]) {
        _mainView.frame = CGRectMake(0, kPFiPhoneAdSpace, _mainView.frame.size.width, _mainView.frame.size.height);
    }
    
    [self.view addSubview:_mainView];
    self.mainView.backgroundColor = THEME_COLOR;
    //self.view.backgroundColor = [UIColor whiteColor];
    
    [GameController sharedGameController].viewController = self;
    [GameKitController sharedGameKitController];
    
    [self.mainView addSubview:[StartView view]];
    //[self addSubViewWithClass:[StartView class]];
    //[self addSubViewWithClass:[ResultView class]];    
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
	[super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
	[super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    //return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
    // 縦向きのみ対応
    if ([kPFDeviceOrientation isEqualToString:@"Portrait"]) {
        return (interfaceOrientation == UIInterfaceOrientationPortrait);
    }else{
    // 横向きのみ対応
        return (interfaceOrientation == UIInterfaceOrientationLandscapeLeft || interfaceOrientation == UIInterfaceOrientationLandscapeRight); 
    }
}

@end
