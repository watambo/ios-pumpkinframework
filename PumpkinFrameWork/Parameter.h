//
//  Parameter.h
//  PFSample
//
//  Created by Kasajima Yasuo on 12/03/05.
//  Copyright (c) 2012年 kyoto. All rights reserved.
//

// 広告のスペース status barがないときは50。あるときは70を設定
#define kPFiPhoneAdSpace 50

// iPhoneの向き Portrait or Landscape
#define kPFDeviceOrientation @"Portrait"
// 広告の位置  PortraitTopかPortraitBottom
#define kPFAdLocation @"PortraitBotom"

// howToBtnがいるかどうか？true or false
#define kPFHowToFlag false

// トランジションの早さ
#define TRANSITION_TOTAL_TIME 1.0

// 背景色　or GrayScale画像を使うときの基本となる色
//#define THEME_COLOR [UIColor colorWithRed:255.0/255.0 green:255.0/255.0 blue:255.0/255.0 alpha:1]
#define THEME_COLOR [UIColor colorWithRed:202.0/255.0 green:231.0/255.0 blue:181.0/255.0 alpha:1]
//#define THEME_COLOR [UIColor colorWithRed:195.0/255.0 green:171.0/255.0 blue:107.0/255.0 alpha:1]

// スコアの順番
#define SCORE_SORT_TYPE @"DESC" // 昇順は@"ASC" 降順は@"DESC"
#define SCORE_COUNT_NAME NSLocalizedString(@"SCORE_COUNT_NAME",nil)

// フォントファミリー
#define FONT_NAME @"HiraKakuProN-W3"
#define FONT_COLOR [UIColor grayColor]

// これより以下は開発時はいじる必要ない。リリースに必要。
/////////////////////
// ソーシャル関係。
////////////////////
#define APP_ID 111111111 //これはitunes connectで登録してから
#define APP_NAME NSLocalizedString(@"APP_NAME",nil)
#define MORE_APP_URL NSLocalizedString(@"MORE_APP_URL",nil)
#define APP_STORE_URL NSLocalizedString(@"APP_STORE_URL",nil)

// Twitter関連
#define TWITTER_HASH_TAG NSLocalizedString(@"TWITTER_HASH_TAG",nil)
#define TWITTER_FOLLOW_ME NSLocalizedString(@"TWITTER_FOLLOW_ME",nil)

#define FACEBOOL_APP_ID @"130738967019784" // Facebook Dialogで必要
#define FACEBOOK_CALLBACK_URL @"http://www.facebook.com/viking.co.ltd" // facebookシェアが終わったら表示される

// GameCenterの設定
#pragma  mark GameCenter
enum LeaderboardID {
    GCLHgighscore = 0,
    GCLTime = 1,
};

static NSString *leaderboardID[] = {
    // 0 ハイスコア用
	@"me.applebite.test.highscore",
};

enum AchievementID {
    GCAScore5 = 0,
    GCAScore10 = 1,
};

static NSString *achievementID[] ={
    // 0 スコアが5に到達
    @"me.applebite.test.score5",  
    // 1 スコアが10に到達
    @"me.applebite.test.score10",  
};