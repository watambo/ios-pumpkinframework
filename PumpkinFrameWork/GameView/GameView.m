//
//  GameView.m
//  PFSample
//
//  Created by Kasajima Yasuo on 12/03/04.
//  Copyright (c) 2012年 kyoto. All rights reserved.
//

#import "GameView.h"
#import "PFHeader.h"


@implementation GameView

// ここからゲーム独自のメソッドを書いていく
#pragma mark Game Method

- (void)update:(NSTimer*)timer{
    NSLog(@"%@",timer.userInfo);
    // タイマーを止める
    [self stopTimer:_cmd];
}



#pragma mark Touch

// タッチ開始
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
//    // タッチされた点を表示
//    UITouch* touch = [touches anyObject];
//	CGPoint pt = [touch locationInView:self];
//    NSLog(@"touch point x:%f, y:%f", pt.x, pt.y);
}
// タッチが動いてる
- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event{
    
}
// タッチが終わった
-(void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event{
    
}
// タッチがキャンセルされた
- (void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event{
    
}


// 画面のサイクル。必要に応じて変更する
#pragma mark Life Cyle
- (void)dealloc {
    // インスタント変数に持たせたものはかならずリリース
    [super dealloc];
}

-(void)startView{
    // 画面遷移の後、かならず呼び出される。トランジションなどはここ。
    [self transitionFadeIn:^{
        // タイマーが必要な場合はこれを書く
        [GameController timerStart];
        // 任意の箇所で以下のコードを書くと、ゲームスタートからの経過時間が取れる
        //double timeFromStart = [GameModel getTime];
        
        NSTimer *tm = [self startTimer:@selector(update:) interval:1.0 withUserInfo:@"test"];
        [tm fire];
    }
     ];
}

- (void)createView{
    self.userInteractionEnabled = YES;
    // ここにゲームの画面の構成を書く。ほかの画面とは違い特にテーマはない
    
    // ゲーム終了ボタン。これは利便的においてあるだけ。
    [self installButtonNamed:@"GameEndBtn" inPosition:CGPointMake(0, 0)];
}


// ここより下は基本的には書き換えない
#pragma mark PressBtn
- (void)pressGameEndBtn:(id)sender{
    // これを任意の場所で呼び出せばゲーム終了
    [GameModel setScore:10]; // ゲームを終了する前にスコアを設定してない場合は必ず設定する
    [GameController gameEnd]; // これがゲームの遷移が始まるメソッド
}

- (void)pressGameResetBtn:(id)sender{
    [GameController gameReset]; // ゲームをリセットしてもう一度ゲームを始めたい場合
}

- (void)pressGameRemoveBtn:(id)sender{
    [GameController gameRemove]; // ゲームをやめてStartViewに戻りたい場合
}

@end
